import unittest
from unittest import TestCase

from main import Calculator


class TestCalculator(TestCase):
    def setUp(self):
        self.calculator = Calculator()

    def test_add(self):
        self.assertEqual(self.calculator.add(4, 7), 11)

    def test_subtract(self):
        self.assertEqual(self.calculator.subtract(10, 5), 5)

    def test_multiply(self):
        self.assertEqual(self.calculator.multiply(3, 7), 21)

    def test_divide(self):
        self.assertEqual(self.calculator.divide(10, 2), 5)

    def test_root(self):
        self.assertEqual(self.calculator.root(9), 3)

    def test_power(self):
        self.assertEqual(self.calculator.power(3, 2), 9)

    def test_ostatok(self):
        self.assertEqual(self.calculator.ostatok(74, 33), 8)

    if __name__ == "__main__":
        unittest.main()
